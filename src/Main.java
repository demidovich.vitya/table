import java.util.Scanner;
import java.util.Date;
import java.util.Random;
public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Введите количество рядов: ");
        short rows = in.nextShort();
        System.out.println();
        System.out.print("Введите количество столбцов: ");
        short columns = in.nextShort();
        System.out.println();
        in.close();
        if ((rows<1) || (rows>1000))
        {
            System.out.println("Количество рядов  меньше 1 или больше 1000");
            return;
        }
        if ((columns<1) || (columns>1000))
        {
            System.out.println("Количество столбцов  меньше 1 или больше 1000");
            return;
        }

        Random random = new Random();
        short range = 10;
        int mas[][] = new int [rows][columns];

        long startTime = System.currentTimeMillis();
        long elapsedTime = 0L;
//        while (elapsedTime < 2*60*1000) {
//            System.out.println("hello \n");
//            //perform db poll/check
//            elapsedTime = new Date().getTime() - startTime;
//        }
        //elapsedTime = new Date().getTime() - startTime;
        // System.out.println("время ожидания: "+elapsedTime);

        for (short i = 0; i < mas.length; i++) {
            for (short j = 0; j < mas[i].length; j++) {

                mas[i][j] = random.nextInt(range);
                if (i == 1 && j == 2) {
                    System.out.print("\t");
                    continue;
                }
                System.out.printf("%2d \t", mas[i][j]);
                elapsedTime = new Date().getTime() - startTime;
                if (elapsedTime > 120000) {
                    System.out.println("\nПревышено время ожидания: "+elapsedTime);
                    return;
                }
            }
            System.out.println();
        }
    }
}
